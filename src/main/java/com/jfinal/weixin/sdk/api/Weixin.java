/**
 * Copyright (c) 2011-2020, James Zhan 詹波 (jfinal@126.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */

package com.jfinal.weixin.sdk.api;

import java.util.Objects;

/**
 * Weixin 工具类用于非 web、非 jfinal 环境下使用，利用 use 方法代替拦截器设定 appid
 * 
 * 使用示例：
 * <pre>
    1：指定 appId 用法：
    ApiResult ret = Weixin.use(appId).call(() -> {
		return MenuApi.getMenu();
	});
	
	使用默认 appId 用法：
	ApiResult ret = Weixin.use().call(() -> {
		return MenuApi.getMenu();
	});
	
 * </pre>
 */
public class Weixin {
	
	protected String appId;
	
	public static Weixin use(String appId) {
		Objects.requireNonNull(appId, "appId can not be null");
		return new Weixin(appId);
	}
	
	public static Weixin use() {
		return new Weixin(null);
	}
	
	Weixin(String appId) {
		this.appId = appId;
	}
	
	public ApiResult call(Call call) {
		if (appId != null) {
			try {
				ApiConfigKit.setThreadLocalAppId(appId);
				return call.run();
			} finally {
				ApiConfigKit.removeThreadLocalAppId();
			}
		}
		else {
			return call.run();
		}
	}
	
	/**
	 * 回调接口 Call，支持 lambda 用法 
	 */
	@FunctionalInterface
	public static interface Call {
		public ApiResult run();
	}
}



